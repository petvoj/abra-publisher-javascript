/// <reference path="../Scripts/typings/jquery/jquery.d.ts" />
declare module Abra.Publisher {
    /**
    * Helper pro generování 'criteria' property v RPC json.
    * Slouží pro stránkování, vyhledávání a filtrování záznamů. Lze jej použít všude tam, kde se dotazujeme metodami "get" a "count" (vrácení kolekce a zjištění velikosti kolekce).
    */
    class CriteriaComposer {
        search: string;
        limit: number;
        offset: number;
        sort: SortComposer;
        filter: FilterComposer;
        constructor();
        withSearch(search: string): CriteriaComposer;
        withLimit(limit: number): CriteriaComposer;
        withOffset(offset: number): CriteriaComposer;
        withSort(sort: SortComposer): CriteriaComposer;
        withFilter(filter: FilterComposer): CriteriaComposer;
        compose(): any;
    }
}
declare module Abra.Publisher {
    /**
    * Helper pro generování 'filter' property v RPC json.
    * Filtrovací podmínky spojované logikou "and" či "or", za použití operátorů "=", "&lt;&gt;", "&lt;", "&gt;", "&gt;=", "&lt;=", "startswith", "endswith", "contains", "in"
    */
    class FilterComposer {
        logic: FilterLogic;
        rules: Array<IRule>;
        constructor();
        withLogic(logic: FilterLogic): FilterComposer;
        withRules(rules: Array<IRule>): FilterComposer;
        compose(): any;
    }
}
declare module Abra.Publisher {
    enum FilterLogic {
        and = 0,
        or = 1,
    }
}
declare module Abra.Publisher {
    interface IRule {
        toRuleObject(): any;
    }
    class FilterRule implements IRule {
        field: string;
        operator: FilterRuleOperator;
        value: any;
        constructor(field: string, operator: FilterRuleOperator, value: any);
        toRuleObject(): {
            field: string;
            operator: string;
            value: any;
        };
        static ToOperatorString(operator: FilterRuleOperator): string;
    }
}
declare module Abra.Publisher {
    enum FilterRuleOperator {
        contains = 0,
        endsWith = 1,
        equalTo = 2,
        greaterThen = 3,
        greaterThenOrEqualTo = 4,
        in = 5,
        lessThen = 6,
        lessThenOrEqualTo = 7,
        notEqualTo = 8,
        startsWith = 9,
    }
}
declare module Abra.Publisher {
    interface IConnection {
        baseUrl: string;
        username: string;
        password: string;
        loginMethod: string;
    }
}
declare module Abra.Publisher {
    interface IArg {
        method: string;
        param: any;
    }
    class JsonRpcClient {
        private connection;
        constructor(connection: IConnection);
        /**
         * Call RPC request
         */
        call<T>(arg: IArg | Array<IArg>): JQueryDeferred<Models.IRpcResponse<T>>;
        private login(connection);
        private post<T>(connection, arg);
        private getData(id, method, param);
    }
}
declare module Abra.Publisher.Models {
    interface IRpcLoginError {
        error: RpcError;
    }
}
declare module Abra.Publisher.Models {
    interface IRpcLoginResponse {
        sessionId: string;
        sessionCookie: string;
        token: string;
        user: RpcUser;
    }
}
declare module Abra.Publisher.Models {
    class RpcError {
        code: number;
        message: string;
        constructor();
    }
}
declare module Abra.Publisher.Models {
    class RpcRequest {
        method: string;
        id: string;
        version: string;
        constructor(method: string, id?: string, version?: string);
    }
}
declare module Abra.Publisher.Models {
    interface IRpcResponse<T> {
        id: string;
        version: string;
        result: T;
        error: RpcError;
    }
    class RpcResponse<T> implements IRpcResponse<T> {
        id: string;
        version: string;
        result: T;
        error: RpcError;
        constructor();
        hasError(): RpcError;
    }
}
declare module Abra.Publisher.Models {
    class RpcUser {
        id: string;
        firstName: string;
        lastName: string;
        constructor();
    }
}
declare module Abra.Publisher {
    class ParamComposer {
        id: any;
        idsWithName: Array<{
            name: string;
            id: any;
        }>;
        extId: any;
        select: string;
        expand: string;
        criteria: CriteriaComposer;
        constructor();
        withId(id: any, paramIdName?: string): ParamComposer;
        withExtId(extId: any): ParamComposer;
        withExpand(expand: string | Array<string>): ParamComposer;
        withSelect(select: string | Array<string>): ParamComposer;
        withCriteria(criteria: CriteriaComposer): ParamComposer;
        compose(): any;
    }
}
declare module Abra.Publisher {
    enum SortDirection {
        asc = 0,
        desc = 1,
    }
}
declare module Abra.Publisher {
    interface ISortItem {
        value: string;
        direction: SortDirection;
    }
    /**
    * Helper pro generování 'sort' property v RPC json.
    */
    class SortComposer {
        fields: Array<ISortItem>;
        constructor();
        withFields(fields: Array<ISortItem>): SortComposer;
        compose(): Object[];
    }
}
