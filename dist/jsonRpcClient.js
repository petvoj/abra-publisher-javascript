var Abra;
(function (Abra) {
    var Publisher;
    (function (Publisher) {
        /**
        * Helper pro generování 'criteria' property v RPC json.
        * Slouží pro stránkování, vyhledávání a filtrování záznamů. Lze jej použít všude tam, kde se dotazujeme metodami "get" a "count" (vrácení kolekce a zjištění velikosti kolekce).
        */
        var CriteriaComposer = (function () {
            function CriteriaComposer() {
            }
            CriteriaComposer.prototype.withSearch = function (search) {
                this.search = search;
                return this;
            };
            CriteriaComposer.prototype.withLimit = function (limit) {
                this.limit = limit;
                return this;
            };
            CriteriaComposer.prototype.withOffset = function (offset) {
                this.offset = offset;
                return this;
            };
            CriteriaComposer.prototype.withSort = function (sort) {
                this.sort = sort;
                return this;
            };
            CriteriaComposer.prototype.withFilter = function (filter) {
                this.filter = filter;
                return this;
            };
            CriteriaComposer.prototype.compose = function () {
                var data = {};
                if (this.search)
                    data.search = this.search;
                if (this.limit && this.limit > 0)
                    data.limit = this.limit;
                if (this.offset && this.offset > 0)
                    data.offset = this.offset;
                if (this.sort)
                    data.sort = this.sort.compose();
                if (this.filter)
                    data.filter = this.filter.compose();
                return data;
            };
            return CriteriaComposer;
        })();
        Publisher.CriteriaComposer = CriteriaComposer;
    })(Publisher = Abra.Publisher || (Abra.Publisher = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Publisher;
    (function (Publisher) {
        /**
        * Helper pro generování 'filter' property v RPC json.
        * Filtrovací podmínky spojované logikou "and" či "or", za použití operátorů "=", "&lt;&gt;", "&lt;", "&gt;", "&gt;=", "&lt;=", "startswith", "endswith", "contains", "in"
        */
        var FilterComposer = (function () {
            function FilterComposer() {
                this.logic = Publisher.FilterLogic.and;
                this.rules = new Array();
            }
            FilterComposer.prototype.withLogic = function (logic) {
                this.logic = logic;
                return this;
            };
            FilterComposer.prototype.withRules = function (rules) {
                var _this = this;
                rules.forEach(function (x) { return _this.rules.push(x); });
                return this;
            };
            FilterComposer.prototype.compose = function () {
                var data = {};
                data.logic = Publisher.FilterLogic[this.logic];
                data.rules = [];
                this.rules.forEach(function (x) { return data.rules.push(x.toRuleObject()); });
                return data;
            };
            return FilterComposer;
        })();
        Publisher.FilterComposer = FilterComposer;
    })(Publisher = Abra.Publisher || (Abra.Publisher = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Publisher;
    (function (Publisher) {
        (function (FilterLogic) {
            FilterLogic[FilterLogic["and"] = 0] = "and";
            FilterLogic[FilterLogic["or"] = 1] = "or";
        })(Publisher.FilterLogic || (Publisher.FilterLogic = {}));
        var FilterLogic = Publisher.FilterLogic;
    })(Publisher = Abra.Publisher || (Abra.Publisher = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Publisher;
    (function (Publisher) {
        var FilterRule = (function () {
            function FilterRule(field, operator, value) {
                this.field = field;
                this.operator = operator;
                this.value = value;
            }
            FilterRule.prototype.toRuleObject = function () {
                return { field: this.field, operator: FilterRule.ToOperatorString(this.operator), value: this.value };
            };
            FilterRule.ToOperatorString = function (operator) {
                switch (operator) {
                    case Publisher.FilterRuleOperator.contains:
                        return "contains";
                    case Publisher.FilterRuleOperator.endsWith:
                        return "endswith";
                    case Publisher.FilterRuleOperator.equalTo:
                        return "=";
                    case Publisher.FilterRuleOperator.greaterThen:
                        return ">";
                    case Publisher.FilterRuleOperator.greaterThenOrEqualTo:
                        return ">=";
                    case Publisher.FilterRuleOperator.in:
                        return "in";
                    case Publisher.FilterRuleOperator.lessThen:
                        return "<";
                    case Publisher.FilterRuleOperator.lessThenOrEqualTo:
                        return "<=";
                    case Publisher.FilterRuleOperator.notEqualTo:
                        return "<>";
                    case Publisher.FilterRuleOperator.startsWith:
                        return "startswith";
                    default:
                        throw new Error("Not implemented filter rule");
                }
            };
            return FilterRule;
        })();
        Publisher.FilterRule = FilterRule;
    })(Publisher = Abra.Publisher || (Abra.Publisher = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Publisher;
    (function (Publisher) {
        (function (FilterRuleOperator) {
            FilterRuleOperator[FilterRuleOperator["contains"] = 0] = "contains";
            FilterRuleOperator[FilterRuleOperator["endsWith"] = 1] = "endsWith";
            FilterRuleOperator[FilterRuleOperator["equalTo"] = 2] = "equalTo";
            FilterRuleOperator[FilterRuleOperator["greaterThen"] = 3] = "greaterThen";
            FilterRuleOperator[FilterRuleOperator["greaterThenOrEqualTo"] = 4] = "greaterThenOrEqualTo";
            FilterRuleOperator[FilterRuleOperator["in"] = 5] = "in";
            FilterRuleOperator[FilterRuleOperator["lessThen"] = 6] = "lessThen";
            FilterRuleOperator[FilterRuleOperator["lessThenOrEqualTo"] = 7] = "lessThenOrEqualTo";
            FilterRuleOperator[FilterRuleOperator["notEqualTo"] = 8] = "notEqualTo";
            FilterRuleOperator[FilterRuleOperator["startsWith"] = 9] = "startsWith";
        })(Publisher.FilterRuleOperator || (Publisher.FilterRuleOperator = {}));
        var FilterRuleOperator = Publisher.FilterRuleOperator;
    })(Publisher = Abra.Publisher || (Abra.Publisher = {}));
})(Abra || (Abra = {}));
/// <reference path="../../typings/jquery/jquery.d.ts" />
var Abra;
(function (Abra) {
    var Publisher;
    (function (Publisher) {
        var JsonRpcClient = (function () {
            function JsonRpcClient(connection) {
                if (!connection)
                    throw new Error("Parameter connection cannot be empty.");
                if (!connection.loginMethod || connection.loginMethod.length === 0)
                    throw new Error("Parameter 'connection.loginMethod' cannot be empty.");
                this.connection = connection;
            }
            /**
             * Call RPC request
             */
            JsonRpcClient.prototype.call = function (arg) {
                var _this = this;
                var deferred = $.Deferred();
                // CALL RPC METHODS
                this.post(this.connection, arg)
                    .then(function (response) {
                    // check error
                    if (response.error === undefined) {
                        // without errors - return result
                        deferred.resolve(response);
                        return;
                    }
                    var rpcResponse = new Publisher.Models.RpcResponse();
                    rpcResponse.error = response.error;
                    // LOGIN TIMEOUT - login again
                    if (rpcResponse.error && rpcResponse.error.code === -32001) {
                        _this.login(_this.connection)
                            .then(function (rpcLoginResponse) {
                            // login error
                            if (rpcLoginResponse.error) {
                                var rpcResponse = new Publisher.Models.RpcResponse();
                                rpcResponse.error = rpcLoginResponse.error;
                                deferred.reject(rpcResponse);
                                return;
                            }
                            // LOGIN SUCCESS
                            // call rpc method after login
                            _this.call(arg)
                                .then(function (response) {
                                deferred.resolve(response);
                                return;
                            }).
                                fail(function (response) {
                                deferred.reject(response);
                                return;
                            });
                        })
                            .fail(function (jqXHR, textStatus, errorThrown) {
                            deferred.reject();
                            return;
                        });
                    }
                    else {
                        deferred.reject(rpcResponse);
                        return;
                    }
                })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                    deferred.reject();
                    return;
                });
                return deferred;
            };
            // LOGIN
            JsonRpcClient.prototype.login = function (connection) {
                var deferred = $.Deferred();
                var param = {
                    user: connection.username,
                    password: connection.password
                };
                var data = this.getData(new Date().valueOf(), connection.loginMethod, param);
                $.ajax({
                    url: connection.baseUrl,
                    type: 'post',
                    data: JSON.stringify(data),
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    xhrFields: {
                        withCredentials: true
                    },
                    success: function (data, textStatus, jqXHR) {
                        deferred.resolve(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        deferred.reject(jqXHR, textStatus, errorThrown);
                    }
                });
                return deferred;
            };
            JsonRpcClient.prototype.post = function (connection, arg) {
                var _this = this;
                var deferred = $.Deferred();
                var counter = 0;
                var now = new Date().valueOf();
                var data = null;
                if (Array.isArray(arg)) {
                    data = new Array();
                    var id = now + (counter++);
                    arg.forEach(function (arg) { return data.push(_this.getData(id, arg.method, arg.param)); });
                }
                else {
                    data = this.getData(now, arg.method, arg.param);
                }
                $.ajax({
                    url: connection.baseUrl,
                    type: 'post',
                    data: JSON.stringify(data),
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    xhrFields: {
                        withCredentials: true
                    },
                    success: function (data, textStatus, jqXHR) {
                        deferred.resolve(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        deferred.reject(jqXHR, textStatus, errorThrown);
                    }
                });
                return deferred;
            };
            JsonRpcClient.prototype.getData = function (id, method, param) {
                return {
                    id: id.toString(),
                    jsonrpc: "2.0",
                    method: method,
                    params: param
                };
            };
            return JsonRpcClient;
        })();
        Publisher.JsonRpcClient = JsonRpcClient;
    })(Publisher = Abra.Publisher || (Abra.Publisher = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Publisher;
    (function (Publisher) {
        var Models;
        (function (Models) {
            var RpcError = (function () {
                function RpcError() {
                }
                return RpcError;
            })();
            Models.RpcError = RpcError;
        })(Models = Publisher.Models || (Publisher.Models = {}));
    })(Publisher = Abra.Publisher || (Abra.Publisher = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Publisher;
    (function (Publisher) {
        var Models;
        (function (Models) {
            var RpcRequest = (function () {
                function RpcRequest(method, id, version) {
                    if (!method || method.length === 0)
                        throw new Error("Parameter 'method' cannot be empty.");
                    this.method = method;
                    this.version = version || "2.0";
                    this.id = id || new Date().valueOf().toString();
                }
                return RpcRequest;
            })();
            Models.RpcRequest = RpcRequest;
        })(Models = Publisher.Models || (Publisher.Models = {}));
    })(Publisher = Abra.Publisher || (Abra.Publisher = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Publisher;
    (function (Publisher) {
        var Models;
        (function (Models) {
            var RpcResponse = (function () {
                function RpcResponse() {
                }
                RpcResponse.prototype.hasError = function () {
                    return (this.error);
                };
                return RpcResponse;
            })();
            Models.RpcResponse = RpcResponse;
        })(Models = Publisher.Models || (Publisher.Models = {}));
    })(Publisher = Abra.Publisher || (Abra.Publisher = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Publisher;
    (function (Publisher) {
        var Models;
        (function (Models) {
            var RpcUser = (function () {
                function RpcUser() {
                }
                return RpcUser;
            })();
            Models.RpcUser = RpcUser;
        })(Models = Publisher.Models || (Publisher.Models = {}));
    })(Publisher = Abra.Publisher || (Abra.Publisher = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Publisher;
    (function (Publisher) {
        var ParamComposer = (function () {
            function ParamComposer() {
                this.idsWithName = new Array();
            }
            // id
            ParamComposer.prototype.withId = function (id, paramIdName) {
                if (paramIdName) {
                    this.idsWithName.push({ name: paramIdName, id: id });
                }
                else {
                    this.id = id;
                }
                return this;
            };
            ParamComposer.prototype.withExtId = function (extId) {
                this.extId = extId;
                return this;
            };
            ParamComposer.prototype.withExpand = function (expand) {
                if (typeof expand === 'string') {
                    this.expand = expand;
                }
                else {
                    this.expand = expand.join(',');
                }
                return this;
            };
            ParamComposer.prototype.withSelect = function (select) {
                if (typeof select === 'string') {
                    this.select = select;
                }
                else {
                    this.select = select.join(',');
                }
                return this;
            };
            ParamComposer.prototype.withCriteria = function (criteria) {
                this.criteria = criteria;
                return this;
            };
            ParamComposer.prototype.compose = function () {
                var data = {};
                if (this.id) {
                    data.id = this.id;
                }
                this.idsWithName.forEach(function (x) {
                    data[x.name] = x.id;
                });
                if (this.extId)
                    data.extId = this.extId;
                if (this.expand)
                    data.expand = this.expand;
                if (this.select)
                    data.select = this.select;
                if (this.criteria)
                    data.criteria = this.criteria.compose();
                return data;
            };
            return ParamComposer;
        })();
        Publisher.ParamComposer = ParamComposer;
    })(Publisher = Abra.Publisher || (Abra.Publisher = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Publisher;
    (function (Publisher) {
        (function (SortDirection) {
            SortDirection[SortDirection["asc"] = 0] = "asc";
            SortDirection[SortDirection["desc"] = 1] = "desc";
        })(Publisher.SortDirection || (Publisher.SortDirection = {}));
        var SortDirection = Publisher.SortDirection;
    })(Publisher = Abra.Publisher || (Abra.Publisher = {}));
})(Abra || (Abra = {}));
/// <reference path="sortdirection.ts" />
var Abra;
(function (Abra) {
    var Publisher;
    (function (Publisher) {
        /**
        * Helper pro generování 'sort' property v RPC json.
        */
        var SortComposer = (function () {
            function SortComposer() {
                this.fields = new Array();
            }
            SortComposer.prototype.withFields = function (fields) {
                var _this = this;
                fields.forEach(function (x) { return _this.fields.push(x); });
                return this;
            };
            SortComposer.prototype.compose = function () {
                return this.fields.map(function (x) { return new Object({
                    field: x.value,
                    dir: Publisher.SortDirection[x.direction]
                }); });
            };
            return SortComposer;
        })();
        Publisher.SortComposer = SortComposer;
    })(Publisher = Abra.Publisher || (Abra.Publisher = {}));
})(Abra || (Abra = {}));
//# sourceMappingURL=jsonRpcClient.js.map